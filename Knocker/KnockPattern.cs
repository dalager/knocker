﻿using System.Linq;

namespace Knocker
{
	public class KnockPattern
	{
		private readonly int[] _raw;
		private const int LongPatternThreshold = 2; // a long pause is at least 2 x a short pause


		public KnockPattern(int[] raw)
		{
			_raw = raw;
		}

		public int[] Raw
		{
			get { return _raw; }
		}


		public int[] GetCleanedUpPauses()
		{
			return _raw.Where(x=>x>100).ToArray();
		}

		public SimplePattern GetSimplePattern()
		{
			var pauses = GetCleanedUpPauses();
			return ConvertToSimplePattern(pauses);
		}

		public static SimplePattern ConvertToSimplePattern(int[] pauses)
		{
			var sp = new SimplePattern();
			var min = pauses.Min(x => x);

			var knockLengths = pauses.Select(i =>
				{
					var factor = (double)i/min;
					var isAboveThreshold = factor >= LongPatternThreshold;
					return isAboveThreshold ? KnockLength.Long : KnockLength.Short;
				});
			
			sp.AddRange(knockLengths);
			return sp;
		}
	}
}
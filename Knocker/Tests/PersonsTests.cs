﻿using Xunit;

namespace Knocker.Tests
{
	public class PersonsTests
	{
		[Fact]
		public void Equality()
		{
			//arrange
			var ps = new Persons();
			var pattern = new SimplePattern() {KnockLength.Short, KnockLength.Long};

			var result = ps.GetMatch(pattern);

			//act
			Assert.NotNull(result);
			Assert.Equal("Christian",result.Name);
			//assert
		}
	}
}
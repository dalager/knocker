﻿using System.Linq;
using Xunit;

namespace Knocker.Tests
{
	public class KnockPatternTests
	{
		[Fact]
		public void CanConvertLongsAndShorts()
		{
			//arrange
			var raw = new[] {196, 400, 500};
			//act
			var sp = KnockPattern.ConvertToSimplePattern(raw);
			//assert
			Assert.Equal(new[]{KnockLength.Short,KnockLength.Long, KnockLength.Long },sp);

		}

		[Fact]
		public void CanConvertToSimplePattern()
		{
			//arrange
			var raw = new[] { 800, 198 };

			//act
			var sp = KnockPattern.ConvertToSimplePattern(raw);

			//assert
			Assert.True(sp.Count() == 2);
			Assert.Equal(KnockLength.Long, sp[0]);
			Assert.Equal(KnockLength.Short, sp[1]);
		}
	
		[Fact]
		public void GetCleanedUpPauses_CleansUpSub100ms_noise()
		{
			//arrange
			var raw = new[] { 800, 80, 198 };

			//act
			var p = new KnockPattern(raw);
			int[] cleanedUpPauses = p.GetCleanedUpPauses();

			//assert
			Assert.Equal(2,cleanedUpPauses.Length);			
		}
	

		[Fact]
		public void Ctor_will_set_Raw_pauses()
		{
			//arrange
			var raw = new[] {800, 198};

			//act
			var p = new KnockPattern(raw);
			
			//assert
			Assert.Equal(raw,p.Raw);

		}
	}
}
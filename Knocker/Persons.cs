﻿using System.Collections.Generic;
using System.Linq;

namespace Knocker
{
	public class Persons : List<Person>
	{
		public Persons()
		{
			AddRange(new List<Person>()
				{
					new Person(){ClassName= "dalager", Name = "Christian Dalager",	Pattern = new SimplePattern(){KnockLength.Short,KnockLength.Long},Logo = "dalager.jpg",Tags=new[]{"eksponent"}},
					new Person(){ClassName= "obrigado", Name = "Obrigado",			Pattern = new SimplePattern(){KnockLength.Short,KnockLength.Short,KnockLength.Long},Logo = "obrigado.png",Tags=new[]{"obrigado"}},
					new Person(){ClassName= "advicedigital", Name = "Advice Digital",Pattern = new SimplePattern(){KnockLength.Short,KnockLength.Long,KnockLength.Long},Logo = "advicedigital.png",Tags=new[]{"advicedigital"}},
					new Person(){ClassName= "advice", Name = "Advice",				Pattern = new SimplePattern(){KnockLength.Short,KnockLength.Short,KnockLength.Short},Logo = "advice.jpg",Tags=new[]{"advice"}},
					new Person(){ClassName= "stranger", Name = "Stranger at the door..",			Pattern = new SimplePattern(){},Logo = "sortslyngel.gif",Tags=new[]{"all"}},
					new Person(){ClassName= "rogerrabbit", Name = "Rabbit!!!",			Pattern = new SimplePattern(){KnockLength.Long,KnockLength.Short,KnockLength.Short,KnockLength.Long},Logo = "rogerrabbit.jpg",Tags=new[]{"all"}},
					new Person(){ClassName= "panic", Name = "Panic at the door!",	Pattern = new SimplePattern(){KnockLength.Short,KnockLength.Short,KnockLength.Long,KnockLength.Long,KnockLength.Long,KnockLength.Short,KnockLength.Short,KnockLength.Short},Logo = "panic.png",Tags=new[]{"all"}},
					new Person(){ClassName= "eksponent", Name = "Eksponent",		Pattern = new SimplePattern(){KnockLength.Long,KnockLength.Short,KnockLength.Long},Logo = "eksponent.png",Tags = new[]{"eksponent"}}
				});
		}

		public Person GetMatch(SimplePattern pattern)
		{
			return this.FirstOrDefault(x => x.Pattern.SequenceEqual(pattern));
		}

	}
}
﻿namespace Knocker
{
	public class Person
	{
		public string Name { get; set; }
		public string ClassName { get; set; }
		public SimplePattern Pattern { get; set; }
		public string[] Tags { get; set; }
		public string Logo { get; set; }
	}
}
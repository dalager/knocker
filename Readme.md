Knocker
=====================

This is the backend for a Netduino door knocker sensor.

The sensor will post pause sequences to /api/knocks as json arrays of ms ints (ie [233,500,323])

The homepage of the app will display realtime notifications.

There's a hardcoded lists of known knock sequences in Persons.cs

You can start the /fakeit page in a separate browser and use the buttons to post data to the service, emulating the Netduino hardware.

The source for the Netduino knock sensor can be found here [Netduino Knock Sensor](https://bitbucket.org/dalager/netduino-knock-sensor)

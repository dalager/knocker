using System;
using Knocker;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using NLog;
using Newtonsoft.Json;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using System.Linq;

namespace KnockService.Models
{
	public class KnocksService:Service
	{

		private Logger _logger;
		private IHubConnectionContext _clients;

		public KnocksService()
		{
			_logger = LogManager.GetCurrentClassLogger();
			_clients = GlobalHost.ConnectionManager.GetHubContext<KnockHub>().Clients;
		}


		public object Post(MaxInput mi)
		{
			_logger.Debug("Value: " + mi.Value);
			_clients.All.maxInput(mi.Value);


			return new MaxInputResponse(){Result = "OK"};
		}

		public object Post(Knocks value)
		{
			_logger.Debug(string.Join(",", value));

			var kp = new KnockPattern(value.ToArray());
			var simplePattern = kp.GetSimplePattern();

			var persons = new Persons();
			var ps = persons.GetMatch(simplePattern);

			if (ps == null)
			{
				ps = persons.First(x => x.ClassName == "stranger");
			}

			var message = string.Join("-", simplePattern);
			//_logger.Debug(ps.Name);
			var times = JsonConvert.SerializeObject(kp.GetCleanedUpPauses());

			_clients.All.knockDetected(ps.Name, times,ps.ClassName);


			return new KnocksResponse() { Result = "OK" };
		}
	}
}
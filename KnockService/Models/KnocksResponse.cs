	using ServiceStack.ServiceInterface.ServiceModel;

namespace KnockService.Models
{
	public class MaxInputResponse
	{
		public string Result { get; set; }
		public ResponseStatus ResponseStatus { get; set; }
	}
	public class KnocksResponse
	{
		public string Result { get; set; }
		public ResponseStatus ResponseStatus { get; set; } //Where Exceptions get auto-serialized
	}
}
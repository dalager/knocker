using System.Collections.Generic;
using ServiceStack.ServiceHost;

namespace KnockService.Models
{
	[Route("/maxinput")]
	public class MaxInput
	{
		public double Value { get; set; }
	}


	[Route("/knocks")]
	public class Knocks : List<int>
	{
		public Knocks()
		{
			
		}
	}
}
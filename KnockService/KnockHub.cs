﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace KnockService
{
	[HubName("signalKnock")]
	public class KnockHub:Hub
	{
		 public void DisplayKnock(string message)
		 {
			 Clients.All.knockDetected(message);
		 }
	}
}
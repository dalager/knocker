﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Knocker;

namespace KnockService.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var ps = new Persons();
			return View(ps);
		}

		public ActionResult About()
		{
			return View();
		}
		public ActionResult Fakeit()
		{
			return View();
		}
	}
}
